package com.shed.shedtool

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class RecordAdapter(val records: ArrayList<Record>) : RecyclerView.Adapter<RecordAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val view = LayoutInflater.from(parent.context).inflate(R.layout.record_list_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return records.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bindRecord(records[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<TextView>(R.id.listViewTitle)
        val text = itemView.findViewById<TextView>(R.id.listViewText)
        val timestamp = itemView.findViewById<TextView>(R.id.listViewTimestamp)
        val category = itemView.findViewById<TextView>(R.id.listViewCategory)
        val btnFavorite = itemView.findViewById<ImageView>(R.id.btnFavorite)


        fun bindRecord(record: Record){
            title?.text = record.title
            text?.text = record.text
            category?.text = record.category
            val dateFormatter = SimpleDateFormat("MMM d, h:mm a", Locale.getDefault())
            if(record.timestamp != null){
                timestamp?.text = dateFormatter.format(record.timestamp)
            }
            onFavoritePress(record,btnFavorite)

            btnFavorite.setOnClickListener {
                FirebaseFirestore.getInstance().collection("shed_tool").document(record.id).update(
                    "favorite",!record.favorite
                )
                onFavoritePress(record,btnFavorite)
            }

        }
    }

    fun onFavoritePress(record: Record,btnFavorite: ImageView){
        if(record.favorite){
            btnFavorite.setBackgroundResource(R.drawable.star_active)
        }else{
            btnFavorite.setBackgroundResource(R.drawable.star_not_active)
        }
    }
}