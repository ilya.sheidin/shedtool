package com.shed.shedtool

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_create_user.*

class CreateUserActivity : AppCompatActivity() {

    lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_user)
        auth = FirebaseAuth.getInstance()
    }

    fun btnRegisterUserClicked(view: View){
        val email = inpRegisterEmail.text.toString()
        val password = inpRegisterPass.text.toString()
        auth.createUserWithEmailAndPassword(email,password)
            .addOnSuccessListener { authResult ->
                // user created
                val username = email.split("@")
                var usernameText: String? = null
                if(username.count() > 1){
                    usernameText = username[0].replace("."," ").replace("_"," ")
                }
                if(usernameText != null){
                    authResult.user.updateProfile(UserProfileChangeRequest.Builder()
                        .setDisplayName(usernameText).build()).addOnFailureListener { exception ->
                        reportError("Could not update user profile ${exception.localizedMessage}")
                    }
                    val data = HashMap<String,Any>()
                    data.put("username",email)
                    data.put("email",usernameText)
                    data.put("created_at",FieldValue.serverTimestamp())
                    FirebaseFirestore.getInstance().collection("users").document(authResult.user.uid)
                        .set(data).addOnSuccessListener {
                            finish()
                            val message = Toast.makeText(this,"Registration complete for user $usernameText", Toast.LENGTH_SHORT)
                            message.show()
                        }.addOnFailureListener { exception ->
                            reportError("Could not update user profile ${exception.localizedMessage}")
                        }
                }

            }
            .addOnFailureListener { exception ->
                reportError("Could not create user ${exception.localizedMessage}")
            }
    }

    fun btnCancelRegisterClicked(view: View){
        finish()
    }

    fun reportError(error: String){
        Log.d("DEBUG",error)
        val message = Toast.makeText(this,error, Toast.LENGTH_LONG)
        message.show()
    }
}
