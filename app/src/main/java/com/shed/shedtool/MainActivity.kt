package com.shed.shedtool

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.*
import kotlinx.android.synthetic.main.activity_add_task.*

import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    private var selectedCategory = ""
    private lateinit var recordAdapter: RecordAdapter
    private val records = ArrayList<Record>()
    private val fireBaseCollection = FirebaseFirestore.getInstance().collection("shed_tool")
    private lateinit var baseCollectionListener : ListenerRegistration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recordAdapter = RecordAdapter(records)
        recordsListView.adapter = recordAdapter
        val layoutManager = LinearLayoutManager(this)
        recordsListView.layoutManager = layoutManager

        val loginIntent = Intent(this,LoginActivity::class.java)
        startActivity(loginIntent)
    }

    override fun onResume() {
        super.onResume()
        setListener()
    }

    fun setListener() {

        if(selectedCategory.isNotEmpty()){
            if(selectedCategory == "Favorite"){
                baseCollectionListener = fireBaseCollection
                    .orderBy("created_at",Query.Direction.DESCENDING)
                    .whereEqualTo("favorite",true)
                    .addSnapshotListener(this) { querySnapshot, firebaseFirestoreException ->
                        if(firebaseFirestoreException != null){
                            reportError("Could not retrieve: $firebaseFirestoreException")
                        }
                        if(querySnapshot != null){
                            updateRecords(querySnapshot)
                        }
                    }
            }else{
                baseCollectionListener = fireBaseCollection
                    .orderBy("created_at",Query.Direction.DESCENDING)
                    .whereEqualTo("category",selectedCategory)
                    .addSnapshotListener(this) { querySnapshot, firebaseFirestoreException ->
                        if(firebaseFirestoreException != null){
                            reportError("Could not retrieve: $firebaseFirestoreException")
                        }
                        if(querySnapshot != null){
                            updateRecords(querySnapshot)
                        }
                    }
            }

        }else{
            baseCollectionListener = fireBaseCollection.
                orderBy("created_at",Query.Direction.DESCENDING)
                .addSnapshotListener(this) { querySnapshot, firebaseFirestoreException ->
                    if(firebaseFirestoreException != null){
                        reportError("Could not retrieve: $firebaseFirestoreException")
                    }
                    if(querySnapshot != null){
                        updateRecords(querySnapshot)
                    }
                }
        }


    }

    fun updateRecords(querySnapshot: QuerySnapshot) {
        records.clear()
        for(document in querySnapshot.documents) {
            val data = document.data
            var date: Date? = null
            if(document.getTimestamp("created_at") != null){
                date = document.getTimestamp("created_at")?.toDate()
            }
            if(!data.isNullOrEmpty() && !data.containsKey("favorite")){
                data["favorite"] = false;
            }

            records.add(Record(document.id,
                data!!["title"] as String,
                data["text"] as String,
                data["category"] as String,
                date as Date,
                data["favorite"] as Boolean
            ))
        }
        recordAdapter.notifyDataSetChanged()
    }

    fun reportError(error: String){
        Log.d("DEBUG",error)
        val message = Toast.makeText(this,error, Toast.LENGTH_LONG)
        message.show()
    }

    fun onCategorySelected() {
        baseCollectionListener.remove()
        setListener()
    }

    fun onBtnAddTaskClick(view: View) {
        val addTask = Intent(this, AddTask::class.java)
        startActivity(addTask)
    }

    fun onbtnViewTasksClick(view: View) {
        selectedCategory = btnViewTasks.text.toString()
        btnViewTasks.isChecked =  true
        btnViewTargets.isChecked = false
        btnViewReminders.isChecked = false
        btnViewFavorites.isChecked = false
        onCategorySelected()
    }

    fun onbtnViewTargetsClick(view: View) {
        selectedCategory = btnViewTargets.text.toString()
        btnViewTasks.isChecked = false
        btnViewTargets.isChecked = true
        btnViewReminders.isChecked = false
        btnViewFavorites.isChecked = false
        onCategorySelected()
    }

    fun onbtnViewRemindersClick(view: View) {
        selectedCategory = btnViewReminders.text.toString()
        btnViewTasks.isChecked = false
        btnViewTargets.isChecked = false
        btnViewReminders.isChecked = true
        btnViewFavorites.isChecked = false
        onCategorySelected()
    }

    fun onbtnViewFavoritesClick(view: View) {
        selectedCategory = btnViewFavorites.text.toString()
        btnViewTasks.isChecked = false
        btnViewTargets.isChecked = false
        btnViewReminders.isChecked = false
        btnViewFavorites.isChecked = true
        onCategorySelected()
    }




}
