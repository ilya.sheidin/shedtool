package com.shed.shedtool

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_add_task.*

class AddTask : AppCompatActivity() {

    var selectedCategory = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_task)
    }

    fun onBtnTasksClick(view: View) {
            selectedCategory = btnTasks.text.toString()
            btnTasks.isChecked = true
            btnTargets.isChecked = false
            btnReminders.isChecked = false
    }

    fun onBtnTargetsClick(view: View) {
        selectedCategory = btnTargets.text.toString()
        btnTasks.isChecked = false
        btnTargets.isChecked = true
        btnReminders.isChecked = false
    }

    fun onBtnRemindersClick(view: View) {
        selectedCategory = btnReminders.text.toString()
        btnTasks.isChecked = false
        btnTargets.isChecked = false
        btnReminders.isChecked = true

    }

    fun onSubmitClick(view: View){
        btnSubmit.isEnabled = false
        if(selectedCategory.isNotEmpty()){
            val data = HashMap<String, Any>()
            data.put("category",selectedCategory)
            data.put("title",inpTitle.text.toString())
            data.put("text",inpText.text.toString())
            data.put("created_at",FieldValue.serverTimestamp())
            FirebaseFirestore.getInstance().collection("shed_tool")
                .add(data)
                .addOnSuccessListener {
                    btnSubmit.isEnabled = true
                    finish()
                }
                .addOnFailureListener {
                    btnSubmit.isEnabled = true
                    val message = Toast.makeText(this,it.message, Toast.LENGTH_SHORT)
                    message.show()
                }
        }else{
            btnSubmit.isEnabled = true
            val message = Toast.makeText(this,"Please select category first", Toast.LENGTH_SHORT)
            message.show()
        }

    }
}
