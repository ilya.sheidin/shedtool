package com.shed.shedtool

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    var clickedForRegister: Int = 0
    lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth = FirebaseAuth.getInstance()
    }

    fun onLoginTextClick(view: View) {
        if(clickedForRegister < 22){
            clickedForRegister += 1
            if(clickedForRegister == 6){
                val message = Toast.makeText(this,"What are you doing?", Toast.LENGTH_SHORT)
                message.show()
            }

            if(clickedForRegister == 14){
                val message = Toast.makeText(this,"Hmmmm are you sure?", Toast.LENGTH_SHORT)
                message.show()
            }

            if(clickedForRegister >= 22){
                val message = Toast.makeText(this,"OK OK! You can register now", Toast.LENGTH_SHORT)
                message.show()
                btnRegisterUser.visibility = View.VISIBLE
            }
        }

    }

    fun btnRegisterUserClicked(view: View){
        val registerIntent = Intent(this,CreateUserActivity::class.java)
        startActivity(registerIntent)
    }

    fun btLoginUserClicked(view: View){
        val email = inpUserEmail.text.toString()
        val password = inpPassword.text.toString()
        auth.signInWithEmailAndPassword(email,password).addOnSuccessListener { AuthResult ->
            val message = Toast.makeText(this,"Logged in as ${AuthResult.user.displayName}", Toast.LENGTH_SHORT)
            message.show()
            finish()
        }.addOnFailureListener { exception ->
            val message = Toast.makeText(this,"Failed to login. ${exception.localizedMessage}", Toast.LENGTH_LONG)
            message.show()
        }
    }
}

