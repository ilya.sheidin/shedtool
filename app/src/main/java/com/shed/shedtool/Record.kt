package com.shed.shedtool

import java.util.*

data class Record constructor(val id: String, val title: String, val text: String, val category: String
                              , val timestamp: Date?, val favorite: Boolean = false)